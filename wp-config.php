<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'amrc2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PB*NOA8L[GL7,&H}v7{U _y@6M$[dk-T*eor$RHc73-Eo[.aEo)KF/+EwMf~r^UI');
define('SECURE_AUTH_KEY',  'iUQt]~ffRv1tRLt+[u2N,_|Vj6>M-q<pX#+F`pP,cx}c)E)JtW8hAvX^BVQDJBKZ');
define('LOGGED_IN_KEY',    '2Ya!(x A5wXJ-QL;&.aS=^pv}CwlBJyC>y0R2l+~F]W#9{in=J -P;[v}.hUo)jN');
define('NONCE_KEY',        've:a()a?Dc5JwV*A*{G!Az@!dzUCm#T@=4hV_-Kp87EDo8$XZjp2}V8}qa^k=,2Z');
define('AUTH_SALT',        'dqWh5{]W0v/8!u:u?yC!y>?=T,gVLDaiYW(ajc-(K*ED9[y)}6Qmuv^*PI5<#&CW');
define('SECURE_AUTH_SALT', '`I/5i2;rdw@`0/k.}T;3}#-ZYGt<U#^<H9d>9i@t@~<Pcz1%:aD2WBVVOkmWyh>-');
define('LOGGED_IN_SALT',   'r:Smv@83B+vX`q-BU++!Az/iC{-Bo5k<oi*?I8<P,=|)Sei/nb2]b[qafsH_||>U');
define('NONCE_SALT',       'Py&GJ1<Bm2s}#|3O2gViX[u=u~|S!:PRfZgjF:&>JLn|l[(i<n9[b00`s*G_FDkM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
